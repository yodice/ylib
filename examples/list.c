#include <algo/list.h>
#include <stdio.h>
#include <mcheck.h>

#define DEBUGGING
#define _GNU_SOURCE 
#define MALLOC_CHECK_ 1

int main(void)
{	yList *l = yList_New();
	void *ptr;
	int i; 
	ptr = &i;

#ifdef DEBUGGING
	mcheck(NULL);
	mtrace();
#endif

	for(i=0; i < 10; i++)
		yList_HPush(l,ptr,sizeof(*ptr));

	for(i=0; i < l->size; i++)
//	while(l->head != NULL)
	{	yElement *el = NULL;
		int *val;

//		el = yList_Pop(l);
		el = yList_Get(l,i);
		val = el->data;

		printf("%d ",*val);
	}
	printf("\n");

#ifdef DEBUGGING
	muntrace();
#endif

	yList_Delete(l);

	return 0;
}
