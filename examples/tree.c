#include <algo/tree.h>
#include <algo/list.h>

#include <stdio.h>
#include <stdlib.h>

#define INT sizeof(int)

yList * Manipulate(void *a_node)
{
	yBTreeNode *the_node = a_node;
	
	if(the_node != NULL)
	{
		int *data_ptr = the_node->data;
		if(data_ptr == NULL)
			data_ptr = malloc(INT);
		*data_ptr = 777;
	}

	return NULL;
}

yList * Display(void *a_node)
{
	yBTree *the_node = a_node;

	if(the_node != NULL)
	{	
		int *data_ptr = the_node->data;
		printf("%p -> %d\n", data_ptr, *data_ptr);
	}

	return NULL;
}

int main(int argc, char *argv[])
{
	yBTree *tree = yBTree_New(malloc(INT), INT);
	yBTreeNode *root = tree;

	yList *search_results = yList_New();

	int *node_value = NULL;

	yBTNode_SetRightSon(root, yBTree_New(malloc(INT), INT));
	yBTNode_SetLeftSon(root, yBTree_New(malloc(INT), INT));	

	yTree_Map(root, Manipulate);
	yTree_Map(root, Display);

	yTree_Delete(tree);

	return EXIT_SUCCESS;
}
