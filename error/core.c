#include "core.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

yList *YERR_Glob_UserErrors = NULL;
yError  *YERR_Glob_LastError = NULL;
char *YERR_Glob_SessionName = NULL;
char *YERR_Glob_DisplayedErrorStr = "/!\\ %s : %s (%s)\n";

static yError * YERR_Get_UserError(const uint16_t yerrno)
{
	yError *yerror = NULL;
	yElement * listelerror = NULL;
	int i=0;
	
	yBool found = False;
	while( found != True && found != undefined )
	{
		listelerror = yList_Get(YERR_Glob_UserErrors, i++);
		
		if(listelerror != NULL)
		{
			yerror = listelerror->data;
			if(yerror->yerrno == yerrno)
				found = True;
		} else
			found = undefined;
	}
	
	return yerror;
}

// I must write YERR_Set_UserErrors

void YERR_Init_Session(const char *name, yError *errors, const int8_t errnum)
{
	yList *errl = yList_New();
	yError undef_error = { YERR_UNDEF, "Undefined error" };
	const int32_t yerr_datasize = sizeof(yError);
	
	yList_HPush(errl, &undef_error, yerr_datasize);
	
	int i;
	for(i=0; i < errnum; i++)
		yList_HPush(errl,&errors[i], yerr_datasize);
	 
	YERR_Glob_UserErrors = errl;
	
	if(YERR_Glob_SessionName != NULL)
		free(YERR_Glob_SessionName);
	
	const int8_t name_slen = strlen(name);
	
	YERR_Glob_SessionName = malloc(name_slen);
	bzero(YERR_Glob_SessionName, name_slen);
	memcpy(YERR_Glob_SessionName, name, sizeof(YERR_Glob_SessionName));
}

void YERR_Display(const uint16_t yerrno)
{
	const yError * yerr = YERR_Get_UserError(yerrno);
	printf(YERR_Glob_DisplayedErrorStr, YERR_Glob_SessionName, yerr->errstr, strerror(errno));
}

void YERR_Fatal(const uint16_t yerrno)
{
	YERR_Display(yerrno);
	printf("[!!] Program quits now\n");
	exit(EXIT_FAILURE);
}

void YERR_Close()
{
	if(YERR_Glob_UserErrors != NULL)
		free(YERR_Glob_UserErrors);
	if(YERR_Glob_LastError != NULL)
		free(YERR_Glob_LastError);
	if(YERR_Glob_SessionName != NULL)
		free(YERR_Glob_SessionName);
	if(YERR_Glob_DisplayedErrorStr != NULL)
		free(YERR_Glob_DisplayedErrorStr);
}
