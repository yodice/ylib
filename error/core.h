#ifndef CORE_H
#define CORE_H

#include <stdint.h>
#include <list.h>

extern yList *YERR_Glob_UserErrors;
//extern yStringList *YERR_Glob_Errors;
extern char *YERR_Glob_SessionName;
extern char *YERR_Glob_ErrorStrBeg;

typedef struct {
	uint16_t yerrno;
	char * errstr;
} yError;

// Add this flag to raise an error that stops program
#define YERR_FATAL_FLAG 0x1000000000000000
#define YERR_UNDEF      0x0000000000000000

#define YERR_Raise(yerrno) \
(yerrno & YERR_FATAL_FLAG) ? YERR_Fatal(yerrno) : YERR_Display(yerrno)

#define YERR_Close_Session(h) \
yList_Delete(h)\
YERR_Close();

extern void YERR_Init_Session(const char *, yError*, const int8_t);
extern void YERR_Fatal(const uint16_t);
extern void YERR_Display(const uint16_t);
// Erases global pointers on heap 
extern void YERR_Close();

#endif
