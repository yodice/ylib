#ifndef PACKET_H
#define PACKET_H

#include <stdint.h>
#include <core.h>

#define YNET_MAX_PKT_SIZE 65536

typedef unsigned int uint;

typedef struct _RawPkt {
	char *data;
	uint16_t size;
} RawPkt;

#define ETH_ALEN 6
#define ETH_HLEN 14

typedef struct _EthernetHdr{
	unsigned char dst[ ETH_ALEN ];
	unsigned char src[ ETH_ALEN ];
	uint	h_proto:2;
} EthHdr;

typedef struct _IpHdr {
	uint version:4; 
	uint hdrlen:4; // Points at the beginning of the data
	uint8_t tos;
	uint16_t len; // Length of header + data (must be greater than 576)
	uint16_t id; // Identifies the sender
	
	/* First bit must be zero
	 * Second bit indicates if fragmentation occurs or not
	 * Last bit inidicates if it is the last fragment or not
	 */ //int flags:3;
	/* Indicates where in the datagram the fragment belongs
	 * First fragment is zero
	 */ uint16_t frag_offset; // Contains flags at his beginning
	 
	uint8_t ttl; // Time to live (seconds)
	uint8_t protocol; // Next level protocol in IP data
	uint16_t hdrchksm;
	uint32_t srcaddr;
	uint32_t dstaddr;
	int option; // Will be implemented further
	
} IpHdr;

typedef struct _TcpHdr {
	uint16_t srcp;
	uint16_t dstp;
	uint32_t seq;
	uint32_t ack;
	uint dataoffset:4; // Number of 32 bits words in the TCP header
	uint reserved:6;
	
	/* URG
	 * ACK
	 * PSH
	 * RST
	 * SYN
	 * FIN : No more data from sender
	 */ uint ctlbits:6;
	 uint16_t window;
	 uint16_t chksm;
	 uint16_t urgptr;
} TcpHdr;

#define YNET_DecodeEthernet_Pkt(rawpkt_ptr) 	(EthHdr *) rawpkt_ptr;
#define YNET_DecodeIP_Pkt(rawpkt_ptr) 		(IpHdr *) rawpkt_ptr;
#define YNET_DecodeTCP_Pkt(rawpkt_ptr)		(TcpHdr *) rawpkt_ptr;

RawPkt * YNET_Wait_Pkt(const int socket);

#endif
