#include "errors.h"
#include <core.h>

void YNET_InitErrors()
{
	yError errors[] = { 	{ ERRYNET_READ_SOCKET, "Can not read on socket" },
									{ ERRYNET_CREATE_SOCKET, "Can not create socket" },
									{ ERRYNET_NOT_CODED_YET, "Not being programmed yet" }
								};

	YERR_Init_Session("libynet", errors, 2);
}