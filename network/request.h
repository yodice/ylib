#ifndef REQUEST_H
#define REQUEST_H

#define DEBUG

#ifdef DEBUG
	#include <stdio.h>
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
//#include <inet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <error.h>
#include <stdlib.h>
#include <core.h>

#define YNET_HTTP_PORT 80

#define YNET_HTTP_VERSION " HTTP/1.1\r\n\r\n"
#define YNET_MAX_BYTES_PER_PAGE 5012
#define YNET_USED_PROTOCOL "tcp"

#define YNET_UA_NAME "ynet/0.1"

#define YNET_HTTP_OPTS_HOST "Host :"
#define YNET_HTTP_OPTS_UA "User-Agent :"

int YNET_TCP_CreateSocket();
char * YNET_HTTP_ReceivePage(const int sockfd);
uint8_t YNET_HTTP_BuildRequest(const char * method, const char * htver, const char *hthost, void **ptr);
uint8_t YNET_HTTP_BuildRequestOption(const char *option, const char *host, void **ptr);
char * YNET_HTTP_Request(const int sockfd, const char*, const char*);
struct sockaddr_in * YNET_InitConnection_WithHostname(const int sockfd, const char*);
struct sockaddr_in * YNET_InitConnection(const int sockfd, struct in_addr *);

#endif
