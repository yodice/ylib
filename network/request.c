#include "request.h"
#include "errors.h"

int YNET_TCP_CreateSocket()
{
	struct protoent * protocol = getprotobynumber(6); 
	const int sockfd = socket(PF_INET, SOCK_STREAM, protocol->p_proto);
//	free(protocol);

	if(sockfd == -1)
		YERR_Raise(ERRYNET_CREATE_SOCKET);

	return sockfd;
}

struct sockaddr_in * YNET_InitConnection_WithHostname(const int sfd, const char * hostname)
{
	struct hostent *host_info;
	struct sockaddr_in *ret = NULL;
	
	host_info = gethostbyname(hostname);
	if(host_info != NULL)
	{
	#ifdef DEBUG
		printf("%s has ip address %s\n", hostname, inet_ntoa( *((struct in_addr *) host_info->h_addr) ) );
	#endif
		ret = YNET_InitConnection(sfd, (struct in_addr *) host_info->h_addr);
	} else fprintf(stderr, "gethostbyname : %s\n", hstrerror(h_errno));

	return ret;
}

struct sockaddr_in * YNET_InitConnection(const int sfd, struct in_addr * target_ip)
{
	struct sockaddr_in * ret = malloc(sizeof(struct sockaddr));

	ret->sin_family = AF_INET;
	ret->sin_port    = htons(YNET_HTTP_PORT);
	ret->sin_addr   = *target_ip;
	bzero(&(ret->sin_zero), 8);

	if(connect(sfd, (struct sockaddr *) ret, sizeof(struct sockaddr)) == -1)
		return NULL;
	
	return ret;
	
}

char * YNET_HTTP_ReceivePage(const int sfd)
{
	int16_t n = 0;
	char * ret, buffer[ YNET_MAX_BYTES_PER_PAGE ];
	
	bzero(&buffer, sizeof(buffer));
	
	char c;
	while(recv(sfd, &c, 1, 0) == 1)
		buffer[ n++ ] = c;
	
	ret = malloc(strlen(buffer));
	
	memcpy(ret, buffer, strlen(buffer));
	
	return ret;
}

uint8_t YNET_HTTP_BuildRequest(const char * htmethod, const char * htversion, const char *hthost, void **ptr)
{
	const uint8_t htmethod_slen  = strlen(htmethod);
	const uint8_t htversion_slen = strlen(htversion);
	const uint8_t req_slen = htmethod_slen + htversion_slen;
	
	if(*ptr == NULL)
		*ptr = malloc(req_slen);
	else if(sizeof(*ptr) < req_slen)
		YERR_Raise(ERRYNET_REQUEST_SIZE);

	void * htversion_beg = *ptr + htmethod_slen;
	
	bzero(*ptr, req_slen);
	memcpy(*ptr, htmethod, htmethod_slen);
	memcpy(htversion_beg, htversion, htversion_slen);
	
	void * hthost_ptr = NULL;
	void * uaname_ptr = NULL;
	
	const uint8_t hthost_slen = YNET_HTTP_BuildRequestOption(YNET_HTTP_OPTS_HOST, hthost, &hthost_ptr);
	const uint8_t uaname_slen = YNET_HTTP_BuildRequestOption(YNET_HTTP_OPTS_UA, YNET_UA_NAME, &uaname_ptr);

	const uint8_t reqwopts_slen = req_slen + hthost_slen + uaname_slen + 1;
	
	void * req_with_options = malloc(reqwopts_slen);
	void * next = req_with_options;

	bzero(req_with_options, sizeof(req_with_options));
	memcpy(next, *ptr, req_slen);
	next = next + req_slen;
	memcpy(next, hthost_ptr, hthost_slen);
	next = next + hthost_slen;
	memcpy(next, uaname_ptr, uaname_slen);

	free(*ptr);
	*ptr = req_with_options;

	return reqwopts_slen;
}

uint8_t YNET_HTTP_BuildRequestOption(const char *htoption_type, const char *htoption, void **ptr)
{
	const uint8_t htoption_t_slen = strlen(htoption_type);
	const uint8_t htoption_slen = strlen(htoption);
	const uint8_t fullhtoption_slen = htoption_t_slen + htoption_slen + 1;
	
	if(*ptr == NULL)
		*ptr = malloc(fullhtoption_slen);

	void * next = *ptr;

	bzero(*ptr, sizeof(ptr));
	memcpy(next, htoption_type, htoption_t_slen);
	next = next + htoption_t_slen;
	memcpy(next, htoption, htoption_slen);
	
	return fullhtoption_slen;
}

char * YNET_HTTP_Request(const int sfd, const char *method, const char *hthost)
{
	void * htreq_ptr = NULL;
	const uint8_t htreq_slen = YNET_HTTP_BuildRequest(method, YNET_HTTP_VERSION, hthost, &htreq_ptr);
	char * htreq = htreq_ptr;

	if(write(sfd, htreq, htreq_slen == -1))
		return NULL;

#ifdef DEBUG
	printf("%s", htreq);
#endif

	free(htreq);

	return YNET_HTTP_ReceivePage(sfd);
}
