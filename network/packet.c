#include "packet.h"
#include "errors.h"
#include <strings.h>
#include <unistd.h>

RawPkt * YNET_Wait_Pkt(const int socket)
{
	static struct _RawPkt pkt;
	char c;
	int n = 0;

	const size_t totalPkt_len = sizeof(struct _EthernetHdr) +
				    sizeof(struct _IpHdr) +
				    sizeof(struct _TcpHdr);

	bzero(&pkt,totalPkt_len);
	
	if(read(socket, &pkt, totalPkt_len) < 0)
		YERR_Raise(ERRYNET_READ_SOCKET);

	return &pkt;
}
