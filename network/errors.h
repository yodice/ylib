#ifndef ERRYNET_H
#define ERRYNET_H

#define ERRYNET_REQUEST_SIZE 		0x0000000000000011
#define ERRYNET_READ_SOCKET 		0x0000000000000111 + YERR_FATAL_FLAG
#define ERRYNET_CREATE_SOCKET	0x0000000000001111 + YERR_FATAL_FLAG
#define ERRYNET_NOT_CODED_YET 	0x0000000000011111 + YERR_FATAL_FLAG

#endif