#ifndef H_MAP
#define H_MAP

#include <element.h>

typedef struct {
	yElement *element;
	yElement *index;
} yMapElement;

//		Push
//
// index : data that indexes the element
// element : element indexed
extern void YALGO_Push_Map(void *index, void *element);

#endif
