#include "tree.h"

#include <malloc.h>
#include <string.h>
//#include <error/core.h>

yBTreeNode * YALGO_New_BTreeNode(const void *data_ptr, 
					const int8_t datasize, yBTreeNode *left, 
					yBTreeNode *right, yBTreeNode *parent)
{
	yBTreeNode *new_node = malloc(sizeof(yBTreeNode));
	new_node->data = malloc(datasize);
	new_node->parent = parent;
	new_node->left = left, new_node->right = right;
	memcpy(new_node->data, data_ptr, datasize);
	return new_node;
}

void YALGO_Delete_Tree(yBTree *t)
{
	yList *l = yList_New();
	yElement *el = NULL;
	
	yList_SPush(l, yTree_Get_Root(t), sizeof(yBTree));
	while( (el = yList_Pop(l)) != NULL)
	{
		yBTreeNode *tn = el->data;
		
		if(el->data != NULL)
		{
			if(tn->left != NULL)
				yList_SPush(l, tn->left,
					sizeof(yBTreeNode));
			if(tn->right != NULL)
				yList_SPush(l, tn->right,
					sizeof(yBTreeNode));
		//free(tn);
		free(el);
		}
	}
}

void YALGO_SetSon_BTreeNode(yBTreeNode *parent, yBTreeNode *son, 
							BinaryMoveCmd location)
{
	son->parent = parent;
	
	if(location == Right)
		parent->right = son;
	else if(location == Left)
		parent->left = son;
	else
		return;
		//YERR_Raise(YALGO_ERR_WRG_CMD);
	
}

void YALGO_MapTree(yBTree *t, MapCallback callback)
{
	if(t != NULL)
	{
		callback(t);

		YALGO_MapTree(t->left, callback);
		YALGO_MapTree(t->right, callback);
	}
}

yTreeResearchResults * YALGO_Browse_Tree(yBTree *t,
						 const void *compared_data, const int16_t datasize)
{
	if(t != NULL)
	{
		static yTreeResearchResults *l = NULL;
		
		if(l == NULL)
			l = yList_New();
	
		if(memcmp(t->data, compared_data, datasize) == 0)
			yList_SPush(l, t, sizeof(yElement));
			
		YALGO_Browse_Tree(t->left, compared_data, datasize);
		YALGO_Browse_Tree(t->right, compared_data, datasize);
		return l;
	}
	return NULL;
}

void YALGO_Dump_ResearchResults(yTreeResearchResults *r)
{
	yElement *el = NULL;
	
	while((el = yList_Pop(r)) != NULL)
	{
		yTreeNode *tn = el->data;
		printf("%p : %p->%s\n", el, tn, tn->data);
	}
}
