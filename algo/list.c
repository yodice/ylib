#include "list.h"
#include <common/mem.h>

#include <stdlib.h>

yElement * YALGO_New_Element()
{
	yElement *new_element = malloc(sizeof(yElement));
	new_element->data = NULL;
	new_element->next = NULL;
	new_element->data_size = 0;
	
	return new_element;
}

static void YALGO_SetPointer_Element(yElement *element, void *new_dataPtr)
{
	element->data = new_dataPtr;
	element->data_size = sizeof new_dataPtr;
}

void YALGO_Delete_List(yList *l)
{
	while(YALGO_IsEmpty_List(l) == False)
			free(YALGO_Pop_List(l));
	
	free(l);
//	l = NULL;
}

yList * YALGO_New_List()
{
	yList * ylist = malloc(sizeof(yList));
	ylist->size = 0;
	ylist->head = NULL;
	ylist->queue = NULL;
	
	return ylist;
}

yBool YALGO_IsEmpty_List(yList *l)
{
	if(l->head == NULL && l->queue == NULL)
		return True;
	else if(l->head != NULL || l->queue != NULL)
		return False;
	else
		return undefined;
}

// Copies data on heap if bool is True
static void YALGO_SetValue_Element(yElement* el, void *new_dataptr, 
									const int8_t datasize, yBool B_hp_alloc)
{
	if(True == B_hp_alloc)
		new_dataptr = YCOMM_CopyOnHeap(new_dataptr, datasize);
	
	YALGO_SetPointer_Element(el, new_dataptr);
}

// Links the n - 1 and n + 1 elements with the new element
void YALGO_Insert_List(yList *l, void *new_el, const int32_t n, 
					const int8_t datasize, yBool B_heap_allocation)
{
	yElement *pre_newel = YALGO_Get_List(l, n-1);
	yElement *post_newel = YALGO_Get_List(l, n);

	yElement * new_yElement = YALGO_New_Element();
	
	YALGO_SetValue_Element(new_yElement, new_el, datasize, 
									B_heap_allocation);
	
	pre_newel->next = new_yElement;
	new_yElement->previous = pre_newel;

	post_newel->previous = new_yElement;
	new_yElement->next = post_newel;
	
	l->size = l->size + 1;
}

void YALGO_Push_List(yList *l, void *new_el, const int8_t datasize, yBool B_heap_allocation)
{
	yElement * new_yElement = YALGO_New_Element();
	
	YALGO_SetValue_Element(new_yElement, new_el, datasize, 
									B_heap_allocation);
	
	if(NULL == l->head)
	{
		l->head = new_yElement;
		l->queue = l->head;
	}
	else
	{
		l->queue->next = new_yElement;
		new_yElement->previous = l->queue;
		l->queue = l->queue->next;
	}
	
	l->size = l->size + 1;
}

// Pointers to element increments for data fetching, this is where this
// implementation lacks of effectivity on large lists
yElement * YALGO_Get_List(yList *l, int32_t n)
{
	if(n >= l->size)
		return NULL;

	yElement *element = NULL;
	yBool from_end = False;
	uint64_t i = 0;
		
	// access time is reduced if the desired 
	// element is after the middle of the list 
	// by beginning at its end
	if(n > l->size/2)
	{
		i = l->size - 1;
		from_end = True;
		element = l->queue;
	} else
		element = l->head;
		
	if(n >= l->size - 1 || n == 0)
		return element;
	
	yBool found = False;
	
	while(found == False)
	{
		if(from_end == True)
			element = element->previous, i--;
		else
			element = element->next, i++;
		
		if(n == i)
			found = True;
	}
	
	return element;
}

// The popped data needs to get destroyed because
// pop only modifies the before last list element
// pointer to next element
yElement * YALGO_Pop_List(yList *l)
{
	if(YALGO_IsEmpty_List(l) != False)
		return NULL;

	yElement *element = l->queue;
	l->queue = l->queue->previous;
	
	if(l->queue != NULL)
		l->queue->next = NULL;
	else
		l->head = NULL;
	
	l->size = l->size - 1;
	
	return element;
}
