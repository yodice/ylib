#ifndef YALGO_TREE_H
#define YALGO_TREE_H

#include "list.h"
#include <stdint.h>

typedef struct {
	void * data;
	void * parent;
	void * right;
	void * left;
} yBTreeNode;

typedef enum { Right, Left, Parent } BinaryMoveCmd;

typedef yBTreeNode yBTree;
typedef yBTreeNode yTreeNode;
typedef yBTreeNode yTree;
typedef yList yTreeResearchResults;

extern yBTree * YALGO_New_TreeNode(const void *, const int8_t, 
						yBTreeNode*,yBTreeNode*, yBTreeNode*);

extern void YALGO_SetSon_BTreeNode(yBTreeNode *, yBTreeNode *, 
						BinaryMoveCmd);

extern void YALGO_Delete_Tree(yBTree*);

extern yTreeResearchResults* YALGO_Browse_Tree(yBTree*, const void *,
										const int16_t);

typedef yList * (*MapCallback)(void *data);

extern void YALGO_MapTree(yBTree *tree, MapCallback); 

extern void YALGO_Dump_ResearchResults(yTreeResearchResults*);

#define yBTree_New(data_ptr, datasize) \
	(yBTree *) YALGO_New_BTreeNode(data_ptr,datasize, NULL, NULL, NULL)

#define yBTNode_New(data_ptr, datasize, left, right, parent) \
	YALGO_New_BTreeNode(data_ptr, datasize, left, right, parent)

#define yTree_Delete(t) YALGO_Delete_Tree(t);

#define yTree_Browse(t, data, datasize) \
	YALGO_Browse_Tree(t, data, datasize);

#define yBTNode_SetRightSon(tnp, tns) \
	YALGO_SetSon_BTreeNode(tnp, tns, Right)

#define yBTNode_SetLeftSon(tnp, tns) \
	YALGO_SetSon_BTreeNode(tnp, tns, Left)

#define yTree_Map(t, callback) YALGO_MapTree(t, callback)

#define yTree_Get_Root(t) (yBTree *) t

#endif
