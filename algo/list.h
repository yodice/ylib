#ifndef YALGO_LIST_H
#define YALGO_LIST_H

#include "element.h"
#include <common/types.h>
#include <stdint.h>

typedef struct {
	yElement * queue;
	yElement * head;
	int size;
} yList;

//		New
// 
// Create a new polymorph list
extern yList * YALGO_New_List();

//		NewFromArray
// 
// pointers : array to be copied
// copy_on_heap : boolean that copies data or not
//
// Copy an array of pointer
extern yList * YALGO_NewFromArray_List(void **pointers, yBool copy_on_heap);

// 		Delete
//
// l : list
//
// Delete each element of the list and the list itself
extern void YALGO_Delete_List(yList* l);

//		Insert
//
// l : list
// new_el : inserted element
// n : insertion index
// copy_on_heap : boolean that copies data or not
//
// Insert a new element at position n in a list
// If n exceeds the list size the element will
// get queued
extern void YALGO_Insert_List(	yList*, void *new_el, const int32_t n,
					const int8_t datasize, yBool copy_on_heap);
//		Push
//
// l : list
// new_el : inserted element
// n : insertion index
// copy_on_heap : boolean that copies data or not
//
// Insert new_el at the end of a list
extern void YALGO_Push_List(	yList* l, void *new_el, const int8_t n,
					yBool copy_on_heap);
//		Pop
//
// l : list
// [!] Will not destroy element [!]
//
// Returns the unlinked last element of a list
// The returned element must be explicitly destroyed
extern yElement * YALGO_Pop_List(yList* l);

//		Get
//
// l : list
// n : index of desired element
//
// Returns the element at position n in a list
extern yElement * YALGO_Get_List(yList* l, const int32_t n);

// 		IsEmpty
//
// l : list
//
// Returns True if the list is empty or False if not
extern yBool YALGO_IsEmpty_List(yList *l);

// Defines for lazy programmers

#define yList_New() YALGO_New_List()
#define yList_NewFromArray(pointers) YALGO_NewFromArray_List(pointers)

#define yList_HInsert(l,xaddr,n,datasize) \
YALGO_Insert_List(l,xaddr,n,datasize, True)

#define yList_SInsert(l,xaddr,n,datasize) \
YALGO_Insert_List(l,xaddr,n,datasize, False)

#define yList_HPush(l,xaddr,datasize) \
YALGO_Push_List(l,xaddr,datasize, True)

#define yList_SPush(l,xaddr,datasize) \
YALGO_Push_List(l,xaddr,datasize, False)

#define yList_Pop(l) YALGO_Pop_List(l)
#define yList_Delete(l) YALGO_Delete_List(l)

#define yList_Get(l,n) YALGO_Get_List(l,n)

#endif
