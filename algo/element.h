#ifndef H_ELEMENT
#define H_ELEMENT

#include <stdint.h>
//#include <map.h>

typedef struct {
	void * data;
	void * next;
	void * previous;
	int data_size;
} yElement;

//              ChangeData
//
// el : element to be changed
// data : data of the element
// st : size type of the element
//
// Updates an element by changing its data and type
extern void YALGO_ChangeData_Element(yElement* el, void* data, int8_t st);

#endif
