#ifndef YCOMM_MEM_H
#define YCOMM_MEM_H

#include <stdint.h>

extern void * YCOMM_CopyOnHeap(void *, int8_t);

#endif