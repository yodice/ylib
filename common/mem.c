#include "mem.h"

#include <stdlib.h>
#include <string.h>

void * YCOMM_CopyOnHeap(void *new_dataPtr, int8_t datasize)
{
	void * new_elementData = NULL;
	
	if(datasize < 1)
		datasize = sizeof new_dataPtr;
	else
		new_elementData = malloc(datasize);
		
	memcpy(new_elementData, new_dataPtr, datasize);
	return new_elementData;
//	YALGO_SetPointer_PolymorphElement(element, new_elementData);
}
