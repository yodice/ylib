# ylib

WHAT IS ?
---------

ylib is an API up to the standard library that implements polymorphism manipulation 
and data structures. A kind of mini-framework to speed-up development in C.

REASONS Y ?
-----------

This project was initiated to write flexible code in C.

I thought that C memory manipulation is a powerful advantage, but sometimes
(most of the time) an harassing task that can overdrive your error tolerance.

That's why I have designed this mini-framework to know what I do with memory,
but with minimizing the effort to write, so maximize the time to think what I do

I found C was lacking of data structures like trees. And I find that trees can
be used to resolve a LOT of reality abstraction.

I want to implement "polymorphic" variables.

And I simply want to have fun !

DEVELOPING
----------

If you have improvement suggestions or simply want to discuss details please contact
me at cocauw@gmail.com

Project is GPL 3.0 licensed
